const products = [
  {
    id: 1,
    name: "iPhone 12 Pro",
    brand: "Apple",
    desc: "6.1-inch display",
    price: 999,
    image:
      "https://res.cloudinary.com/ddkzgx6ql/image/upload/v1698641436/mwyv1ychvk52kkloicbf.jpg",
  },
  {
    id: 2,
    name: "iPhone 12",
    brand: "Apple",
    desc: "5.4-inch mini display",
    price: 699,
    image:
      "https://res.cloudinary.com/ddkzgx6ql/image/upload/v1698642001/artes3rhtewcm0tblspw.jpg",
  },
  {
    id: 3,
    name: "Galaxy S",
    brand: "Samsung",
    desc: "6.5-inch display",
    price: 399,
    image:
      "https://res.cloudinary.com/ddkzgx6ql/image/upload/v1698655946/tpkkksts5mgwbawgoz18.jpg",
  },
];

module.exports = products;
